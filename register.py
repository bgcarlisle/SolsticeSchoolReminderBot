from mastodon import Mastodon

Mastodon.create_app(
     'Solstice School Reminder Bot',
     api_base_url = 'https://scholar.social', ## Change this
     to_file = 'pytooter_clientcred.secret'
)
